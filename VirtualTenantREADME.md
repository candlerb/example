# Virtual Tenant Deployment

The following instructions deploy the SMARTER demo into a multi-tenant environment, assuming the user acts as both the infrastructure provider and an example tenant of the system.

![alt text](images/Multi-Tenant.png "Virtual Node Data Flow")*In the above diagram the orange lines represent sensor data and the yellow lines indicate tcp based communication*

Checkout the repository:
```
git clone https://gitlab.com/arm-research/smarter/example.git
cd example
```

## Create a virtual node using the SMARTER Brokerage Manager
To create a virtual node use the following [instructions](https://gitlab.com/arm-research/smarter/smarter-brokerage#running-an-example-of-smarter-brokerage-manager). At the end of this process, you should have a virtual node registered to your example tenants k3s cluster. You should see the node is ready by running, with your KUBECONFIG set properly to your **tenant** k3s cluster:
```bash
kubectl get nodes -o wide
```

## Deploy System Services (Shared by Tenants)
Using your KUBECONFIG which points to your **physical** node cluster we will deploy the system level services into the cluster.

Enable smarter CNI, DNS, and Device Manager by running the following from the demo directory on your dev machine:
```
cd demo
./start_smarter.sh
cd ..
```

If you are running on a device not using our Yocto images you need to manually label the node.
For Jetsons and Raspberry Pi 4's beyond version 0.6.4.1 of the yocto build this happens during initialization and does not need to be done manually.

* `smarter.cni`: this label enables the deployment of the SMARTER CNI. 
* `smarter.cri`: this label controls the deployment of the SMARTER DNS, ensuring that the smarter-dns deployed is configured to query the k3s docker CRI.
* `smarter-device-manager`: this label enables the smarter device manager to run on your device

Run the following on your dev machine:
```bash
    kubectl label node ${NODE_TO_USE} smarter.cni=deploy
    kubectl label node ${NODE_TO_USE} smarter.cri=docker
    kubectl label node ${NODE_TO_USE} smarter-device-manager=enabled
```

Export the following names in your env on your dev machine:
```bash
export KUBECONFIG=<YOUR_SYSTEM_KUBECONFIG>
export NODE_TO_USE=<YOUR_PHYSICAL_NODE_NAME>
```

If you are running the demo on a **Raspberry Pi**, please label your node by doing the following:
```bash
kubectl label node ${NODE_TO_USE} smarter.nodetype=raspberrypi4
```

If you are running the demo on a **JETSON DEVICE (Xavier/Nano)**, please label your node by doing the following, such that gpu enabled containers will be used where possible:
```bash
kubectl label node ${NODE_TO_USE} smarter.nodetype=jetson
```

### Deploy GStreamer and Pulseaudio workloads
```bash
cd demo

# Deploy Pulseaudio
kubectl apply -f ../yaml/pulseaudio/

# Label node to run Pulseaudio daemon
kubectl label node ${NODE_TO_USE} pulse=enabled

# Deploy GStreamer
kubectl apply -f ../yaml/gstreamer/

# Label node to run GStreamer
kubectl label node ${NODE_TO_USE} gstreamer=enabled
```

### Deploy Triton ML Server
To enable dynamic control of our ML workloads we use [nvidia triton](https://github.com/NVIDIA/triton-inference-server) to serve http or grpc inference requests from clients.
At this point and time triton runs on both jetson cuda enabled devices and arm cpu/gpu devices (with ArmNN support).
Using triton allows both the audio classifier and image detector to share the cuda gpu.

```bash
# Deploy Triton
kubectl apply -f ../yaml/triton/

# Label node to run triton model server
kubectl label node ${NODE_TO_USE} triton=enabled
```

### Deploy Non-Jetson Device specific workloads
At this point in time we do not have the ability to run the audio classification logic on triton with armnn support, therefore we will rely on a separate ML server from IBM. This workload will only run if you are **NOT** running on a Jetson platform as indicated by the label `smarter.nodetype=jetson`.
```
# Deploy ambient sound classifier
kubectl apply -f ../yaml/ambient-sound-classifier/ambient-sound-classifier-ds.yaml

# Label node to run ambient sound classifier
kubectl label node ${NODE_TO_USE} ambient-sound-classifier=enabled
```

## Deploy Client Workloads on Virtual Node

Note that at the moment workloads running on the virtual node will not be able to access the GPU. Also, we will not support device passthrough to virtual nodes for the purposes of this demo.

Export the following names in your env on your dev machine:
```bash
export KUBECONFIG=<YOUR_TENANT_KUBECONFIG>
export NODE_TO_USE=<YOUR_VIRTUAL_NODE_NAME>
export SMARTER_DATA_DOMAIN=<YOUR_K3S_DATA_NODE_IP (DASH SEPARATED)>.nip.io
export SMARTER_EDGE_DOMAIN=<YOUR_TENANT_K3S_EDGE_MASTER_IP>
export SMARTER_EDGE_PORT=<YOUR_TENANT_K3S_EDGE_MASTER_PORT>
```

Enable smarter CNI, DNS, and Device Manager by running the following from the demo directory on your dev machine:
```
cd demo
./start_smarter.sh
cd ..
```

If you are running on a device not using our Yocto images you need to manually label the node.
For Jetsons and Raspberry Pi 4's beyond version 0.6.4.1 of the yocto build this happens during initialization and does not need to be done manually.

* `smarter.cni`: this label enables the deployment of the SMARTER CNI. 
* `smarter.cri`: this label controls the deployment of the SMARTER DNS, ensuring that the smarter-dns deployed is configured to query the k3s docker CRI.
* `smarter-device-manager`: this label enables the smarter device manager to run on your device

Run the following on your dev machine:
```bash
    kubectl label node ${NODE_TO_USE} smarter.cni=deploy
    kubectl label node ${NODE_TO_USE} smarter.cri=docker
    kubectl label node ${NODE_TO_USE} smarter-device-manager=enabled
```

Make sure your kubectl is targeting your Tenant k3s cluster for the following steps, **NOT** your physical node k3s cluster.

Obtain your fluentd password by running the following command against your **cloud** k3s instance:
```bash
kubectl get secrets/fluentd-credentials --template={{.data.password}} | base64 -d
```
Register your cloud fluentd credentials in your **edge** cluster by running:
```bash
kubectl create secret generic fluentd-credentials --from-literal=password=<YOUR FLUENTD PASSWORD>
```

If you are running the demo on a virtual **Raspberry Pi**, please label your node by doing the following:
```bash
kubectl label node ${NODE_TO_USE} smarter.nodetype=raspberrypi4
```

If you are running the demo on a virtual **JETSON DEVICE (Xavier/Nano)**, please label your node by doing the following, such that gpu enabled containers will be used where possible:
```bash
kubectl label node ${NODE_TO_USE} smarter.nodetype=jetson
```

### Deploy base workloads
To run demo perform the following steps on your dev machine, **from the demo directory**:
```bash
cd demo

# Use standard kubernetes CLI to get information about nodes connected to our cluster
kubectl get nodes -o wide
kubectl get node ${NODE_TO_USE} -o wide --show-labels

# Deploy fluent-bit DaemonSets and ConfigMaps
kubectl apply -f ../yaml/fluent-bit/ 2>/dev/null
# Update fluent-bit-ds with server IP locations
envsubst < ../yaml/fluent-bit/fluent-bit-ds.yaml | kubectl apply -f -
kubectl get ds

# Label node to run fluent-bit
kubectl label node ${NODE_TO_USE} fluent-bit=enabled
kubectl get node ${NODE_TO_USE} -o wide --show-labels

# Check status of pods using the following command, when pod is running, proceed to the next step
kubectl get pods --field-selector spec.nodeName=${NODE_TO_USE} -o wide

# Deploy Netdata monitoring agent
kubectl apply -f ../yaml/netdata 2>/dev/null
envsubst < ../yaml/netdata/netdata-ds.yaml | kubectl apply -f -
kubectl label node ${NODE_TO_USE} netdata-monitor=enabled

# Check status of pod using the following command, when pod is running, proceed to the next step
kubectl get pods --field-selector spec.nodeName=${NODE_TO_USE} -o wide
```

### Deploy ML clients
```bash
# Deploy the Sensor data collection applications
kubectl apply -f ../yaml/image-detector/image-detector-ds.yaml
kubectl apply -f ../yaml/audio-client/audio-client-ds.yaml

# Label node to run image detector and web server
kubectl label node ${NODE_TO_USE} image-detector=enabled

# Check status of pods using the following command, when pods are running, proceed to the next step
kubectl get pods --field-selector spec.nodeName=${NODE_TO_USE} -o wide

# Label node to run sound classifier and audio client
kubectl label node ${NODE_TO_USE} audio-client=enabled

# Check status of pods using the following command, when pods are running, proceed to the next step
kubectl get pods --field-selector spec.nodeName=${NODE_TO_USE} -o wide
```

You can check the status of the pods on NODE_TO_USE by running the command `kubectl get pods --field-selector spec.nodeName=${NODE_TO_USE} -w -o wide`.

```bash
# Deploy the updated image detector for both cars and pedestrians
kubectl apply -f ../yaml/image-detector/image-detector-extended-ds.yaml
kubectl get pods --field-selector spec.nodeName=${NODE_TO_USE} -o wide
```

Login to your grafana instance (at https://grafana-${SMARTER_DATA_DOMAIN}) On the left menu select the plus and from the dropdown select import, then select `Upload .json file` and choose the file `dashboards/smarter-grafana-demo-dashboard.json` in this repo. For the prometheus data source select `Prometheus` and for the influxdb data source select `InfluxDB-Waggle` and press import. In this dashboard you should now be able to see your data streaming for car/people detection and sound classification. You can switch which node you are interested in querying by using the dropdown in the top left corner of the dashboard. Note that the bottom chart is only designed to show data when it detects sounds of interest to a smarter-city application (i.e. Gunshot, Car Engine, etc).

To install other useful dashboards to track your cloud clusters application performance, select the plus on the left menu and then import once again, but this time enter the dashboard id `10346` for influx db metrics. You will now see a dashboard named `InfluxDB OSS Stats monitoring dashboard`.

We can also monitor fluentd by importing dashboard id `9950` and for the data source selection choose `prometheus`.

When you are ready clean up the entire demo:
```bash
./clean_demo_fast
```
This script will remove all node labels applied for the demo and remove all kubernetes resources applied to cluster.

To ensure that all pods were removed you can repeatedly run `kubectl get pods --field-selector spec.nodeName=${NODE_TO_USE} -o wide`.

## Troubleshooting
**Common Problems**

| Problem | Solution |
| ------ | ------ |
| "A pod is not running when I think it should be" | Check that the desired daemonset is applied and there is in fact a non-zero number listed in the `desired column` by running `kubectl get ds -o wide`. Check that your node is labeled properly by running `kubectl get node ${NODE_TO_USE} -o wide --show-labels`. I have seen scenarios in the past where if you apply a dameonset first, then apply a configmap which the daemonset needs, no pods for that daemonset will run. To fix this you must delete and reapply the daemonset. |
| "I updated the configmap for an app, why isn't the pod re-spawning with the new config?" | Kubernetes does not restart a pod when a configmap it sources is updated, you must update the config map, then delete and reapply the daemonset to get the changes. | 
| "Grafana is not displaying any data in the graph" | The grafana chart is designed to show data only from one specific node for the time being. If you do not set the nodename variable as specified in the instructions above, you will not see any data in the chart. Make sure you have set your endpoints for influxdb appropriately in the fluent-bit configmap as specified above. |
| "The audio-client container keeps restarting" | This is more likely than not because it is unable to get microphone data from the pulseaudio container. Ensure the pulseaudio container is running, and that the file /etc/modprobe.d/alsa-base.conf exists on the host filesystem for the node. And ensure the usb microphone is connected properly. | 
| "The local path provisioner container continually restarts" | There is no need to worry about this issue for the time being. It does not affect the functionality of the demo. |
| "The pulseaudio pod is stuck in pending" | Check the logs of the device manager pod running on your node. If it is complaining about connectivity issues, be sure to verify that you are running k3s-agent version >= 1.18.3. You can check your agent version by running `kubectl get node -o wide`.
| "The audio client is running but there's no classification data in grafana" | Occasionally the pyaudio library seems to be unable to read data from the microphone via tcp from pulseaudio. If this is the case unplug and re-plug in the mic and run the pods again.

