#!/bin/bash
# Copyright (c) 2020, Arm Ltd
# Author: Luis E. P.
# Modifications by: Eric V. H., Josh Minor
#
# This script will setup a k3s master on this machine

set -e

if [ -z $MY_EMAIL ]
then
    echo "PLEASE SET THE ENV VARIABLE MY_EMAIL"
    exit 1
fi

export REPO_HOME=$(pwd)

# This is the k3s version to be downloaded and installed
export K3S_VERSION="v1.18.3+k3s1"

# Log for installation. Useful for debugging
export INSTALLATION_LOG="logs/cloud-k3s-setup-"$(date +"%Y%m%d%H%M%S").log

# Create logs directory if doesn't exist...
mkdir -p logs

# Starting logging bracket...
{

# We need to set the advertise IP to the IP other nodes can use to reach the k3s master
# Depending on where the k3s master is running, the advertise IP is the same as the internal IP or
# it is a different, external IP. The latter case is common when running in cloud machines.
#
# More info on how to get the IP:
# https://stackoverflow.com/questions/23362887/can-you-get-external-ip-address-from-within-a-google-compute-vm-instance
# 
# cloud-init is a helpful tool to determine which way we detect external address

echo "========================================================================="
echo "Trying to determine if this machine is running in the cloud by using cloud-init"
type cloud-init
if [ $? -eq 0 ]
then
    echo "cloud-init is available!"

    echo "Getting platform info from cloud-init"
    export PLATFORM=$(sudo cloud-init query platform)

    if [ $? -ne 0 ]
    then
        echo "cloud-init failed, exiting script..."
        exit -1
    fi

    case ${PLATFORM} in
        ec2)
            # Use this export if AWS EC2
            echo "Running on AWS EC2"
            export ADVERTISE_IP=$(curl http://169.254.169.254/latest/meta-data/public-ipv4) || (echo "Not able to get IP address from AWS";exit -1)
            ;;

        gce)
            # Use this export if Google GCE
            echo "Running on Google GCE"
            export ADVERTISE_IP=$(curl -H "Metadata-Flavor: Google" http://169.254.169.254/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip)
            ;;

            # Use this export if Azure
            # export ADVERTISE_IP=$(curl -H Metadata:true "http://169.254.169.254/metadata/instance/network/interface/0/ipv4/ipAddress/0/publicIpAddress?api-version=2017-08-01&format=text")

        *) 
            # Use this export if external IP equals internal IP
            echo "'${PLATFORM} unknown, assuming local server"
            export ADVERTISE_IP=$(hostname -I | awk '{print $1;}')
            export LOCAL=1
            ;;

    esac
else
    echo "cloud-init not available, assuming local server"
    export ADVERTISE_IP=$(hostname -I | awk '{print $1;}')
    export LOCAL=1
fi
export SMARTER_DATA_DOMAIN=${ADVERTISE_IP//./-}.nip.io
export THIS_HOST_IP=$(hostname -I | awk '{print $1;}')

echo "Using $ADVERTISE_IP as the k3s master advertise address..."
echo "Using $THIS_HOST_IP as the local bind address..."
echo "Please confirm this is the IP the nodes can use to reach this k3s master!"
echo "========================================================================="

# Store the advertise address in a file for later use
echo $ADVERTISE_IP > server_ip

# Run k3s-uninstall if it exists
echo "\nRemoving k3s if currently installed...\n"
[ -x /usr/local/bin/k3s-uninstall.sh ] && /usr/local/bin/k3s-uninstall.sh

# Store the k3s version in a file for later use
echo $K3S_VERSION > k3s_version

echo "========================================================================="
echo "Installing k3s $K3S_VERSION using the k3s install.sh script..."
echo "========================================================================="

curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=$K3S_VERSION sh -s - --write-kubeconfig-mode 664 --bind-address $THIS_HOST_IP --advertise-address $ADVERTISE_IP | tee -a ${INSTALLATION_LOG}

echo "========================================================================="
echo "Copying the kubeconfig to $PWD/cloud-kube.config..."
sudo cat /etc/rancher/k3s/k3s.yaml > cloud-kube.config

echo "Updating the address in cloud-kube.conifg to the advertise address..."
sed -i "s/$THIS_HOST_IP/$ADVERTISE_IP/g" cloud-kube.config

echo "Copying the node-token to $PWD/token..."
sudo cat /var/lib/rancher/k3s/server/node-token > token

echo "========================================================================="
echo "kubectl cluster-info"
export KUBECONFIG=$PWD/cloud-kube.config
/usr/local/bin/k3s kubectl cluster-info --kubeconfig $PWD/cloud-kube.config

echo "====="
echo "Waiting for all k3s base containers to come up..."
echo "====="

sleep 60
/usr/local/bin/k3s kubectl get all -A

echo "====="
echo "Deleting previous .helm directory"
rm -rf ${HOME}/.helm
echo "====="

echo "====="
echo "Installing latest Helm 2 using their convenience script"
echo "====="
curl -fsSl https://raw.githubusercontent.com/helm/helm/master/scripts/get -o get-helm2.sh
sudo bash get-helm2.sh

echo "====="
echo "Creating RBAC definitions for HELM"
echo "====="
/usr/local/bin/k3s kubectl create serviceaccount --namespace kube-system tiller
/usr/local/bin/k3s kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller

echo "====="
echo "Initializing Helm with $KUBECONFIG config"
echo "====="
helm init --service-account tiller --kubeconfig $KUBECONFIG --wait

echo "====="
echo "Waiting for tiller to come up..."
echo "====="

sleep 5

echo "========================================================================="
echo "Installing Cert-Manager"
kubectl apply --validate=false -f https://raw.githubusercontent.com/jetstack/cert-manager/v0.13.0/deploy/manifests/00-crds.yaml
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
if [ -z $LOCAL ]
then 
    helm install \
        --wait \
        --timeout 500 \
        -f yaml/cert-manager/cert-manager-values.yaml \
        --name cert-manager \
        --namespace cert-manager \
        --version v0.13.0 \
        jetstack/cert-manager
else
    helm install \
        --wait \
        --timeout 500 \
        -f yaml/cert-manager/cert-manager-values-local.yaml \
        --name cert-manager \
        --namespace cert-manager \
        --version v0.13.0 \
        jetstack/cert-manager
fi

envsubst < yaml/cert-manager/letsencrypt-production-issuer.yaml | kubectl apply -f -
sleep 10


echo "========================================================================="
echo "Installing InfluxDB"

helm install --wait --timeout 500 --kubeconfig $KUBECONFIG -f yaml/influxdb/influxdb-values.yaml --name influxdb stable/influxdb

echo "========================================================================="
echo "Installing Elasticsearch"

# Install docker first
sudo apt remove -yqq docker docker-engine docker.io containerd runc || true
sudo apt update -yqq
sudo apt install -yqq \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common \
    make
    
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
   
sudo apt update -yqq
sudo apt install -yqq docker-ce docker-ce-cli containerd.io

# Create secrets for elasticsearch and kibana
rm -rf ~/helm-charts || true
cd ~
git clone https://github.com/elastic/helm-charts
cd ~/helm-charts/elasticsearch/examples/security
sudo make secrets
cd ~/helm-charts/kibana/examples/security
sudo make secrets
cd $REPO_HOME

helm repo add elastic https://helm.elastic.co
helm repo update
envsubst '${SMARTER_DATA_DOMAIN}' < yaml/elasticsearch/elasticsearch-values.yaml > yaml/elasticsearch/elasticsearch-custom.yaml
helm install --wait --timeout 500 -f yaml/elasticsearch/elasticsearch-custom.yaml --name elasticsearch elastic/elasticsearch

echo "========================================================================="
echo "Installing Kibana"
envsubst '${SMARTER_DATA_DOMAIN}' < yaml/kibana/kibana-values.yaml > yaml/kibana/kibana-custom.yaml
helm install --wait --timeout 500 -f yaml/kibana/kibana-custom.yaml --name kibana elastic/kibana

echo "========================================================================="
echo "Installing Prometheus+Grafana"
kubectl create secret generic grafana-credentials --from-literal=admin-password=$(openssl rand -base64 8 | tr -dc A-Za-z0-9) --from-literal=admin-user=admin
envsubst '${SMARTER_DATA_DOMAIN}' < yaml/prometheus/prometheus-values.yaml > yaml/prometheus/prometheus-custom.yaml
helm install --wait --timeout 500 -f yaml/prometheus/prometheus-custom.yaml --name prometheus-operator stable/prometheus-operator

echo "========================================================================="
echo "Installing Fluentd"
kubectl apply -f yaml/cert-manager/selfsigned-issuer.yaml
sleep 10
envsubst < yaml/cert-manager/fluentd-certificate.yaml | kubectl apply -f -
sleep 10

kubectl create secret generic fluentd-credentials --from-literal=password=$(openssl rand -base64 8 | tr -dc A-Za-z0-9)
helm install --wait --timeout 500 -f yaml/fluentd/fluentd-values.yaml --name fluentd stable/fluentd

/usr/local/bin/k3s kubectl get all -A
echo "========================================================================="
echo "Grafana initial admin password has been set to $(kubectl get secret grafana-credentials -o jsonpath='{.data.admin-password}' | base64 --decode)"
echo "NOTE: If using kubectl from this machine, make sure to run 'export KUBECONFIG=$PWD/cloud-kube.config'"
echo "REMEMBER: To access grafana use https://grafana-$SMARTER_DATA_DOMAIN"
echo "REMEMBER: To access kibana use https://kibana-$SMARTER_DATA_DOMAIN"
echo "REMEMBER: To access prometheus dashboard use https://prometheus-$SMARTER_DATA_DOMAIN"
echo "NOTE: on your dev machine export SMARTER_DATA_DOMAIN=$SMARTER_DATA_DOMAIN"
echo "Your elasticsearch and kibana username is 'elastic' with password $(kubectl get secret elastic-credentials -o jsonpath='{.data.password}' | base64 --decode)"
echo "========================================================================="
} 2>&1 | tee ${INSTALLATION_LOG}
# Ending debugging bracket...


