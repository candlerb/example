# Overview
This document will help you run a Smarter k3s master  

# Running on docker

## System requirements

### k3s edge master
* Local linux (x86_64 or arm64)/windows/MacOS machine with docker, AWS EC2 VM instance or Google Cloud Platform GCE VM instance
* Multiple k3s edge masters can be run in a single server if different server ports are used (HOSTPORT).

### dev machine
* User's desktop, capable of ssh'ing to the k3s edge master host

## Network topology
* The k3s master host and the dev machine both need access to the Internet.
* The dev machine needs to be able to `ssh` and `scp` into the k3s master host.
* The k3s master needs to have port 6443 (or the port that is desired to run k3s on) open for k3s.
* The edge node needs to have access to port 6443 (or the port that is desired to run k3s on) in the k3s master.

## Setting k3s master up

The repository [K3S on gitlab SMARTER](ihttps://gitlab.com/arm-research/smarter/k3s.git) contains a docker image that allows k3s to run as container.
Execute the following command to download the file:
```
wget https://gitlab.com/arm-research/smarter/k3s/-/jobs/702137534/artifacts/raw/package/smarter-image/k3s-start.sh
```

The file [k3s-start.sh](https://gitlab.com/arm-research/smarter/k3s/-/jobs/702137534/artifacts/raw/package/smarter-image/k3s-start.sh) provides a script that allows the user to start the container with the correct options.
Rename it if starting multiple servers so each one can have its own start script, change variables HOSTPORT, HOSTIP and SERVERNAME so the script will start the SMARTER Node Manager with the correct configuration for your environment.
The SERVERIP should be the IP that the nodes will be connected. if NAT is used use the IP of the NAT and not the IP of the server.

execute the script:
```
./k3s-start.sh
````

THe following is a result of a successful run
```
Unable to find image 'registry.gitlab.com/arm-research/smarter/k3s:v1.18.3-k3s2-smarter' locally
v1.18.3-k3s2-smarter: Pulling from arm-research/smarter/k3s
21c83c524219: Already exists
0fa81250dd09: Pulling fs layer
4ff9f24d1484: Pulling fs layer
eca4c3fde112: Pulling fs layer
9f402bfc611d: Pulling fs layer
9f402bfc611d: Waiting
eca4c3fde112: Verifying Checksum
eca4c3fde112: Download complete
0fa81250dd09: Verifying Checksum
0fa81250dd09: Download complete
9f402bfc611d: Verifying Checksum
9f402bfc611d: Download complete
0fa81250dd09: Pull complete
4ff9f24d1484: Verifying Checksum
4ff9f24d1484: Download complete
4ff9f24d1484: Pull complete
eca4c3fde112: Pull complete
9f402bfc611d: Pull complete
Digest: sha256:2fffb2d79c803830ecc2eeddd0878b1d83ac26c4ebd0be3952bfba57e588d045
Status: Downloaded newer image for registry.gitlab.com/arm-research/smarter/k3s:v1.18.3-k3s2-smarter
Container ID: 4a8293d2965cb2e5bd08da307cd1ae929b61f41a18c219a647d002bc1601877c
waiting for the container to generate the secrets
Copying the secrets, token to token.tenant1 and kube.confg to kube.tenant1.config
```
The files token.\<SERVERNAME\> and kube.\<SERVERNAME\>.config contains the credentials to be use to authenticate a node (token file) or kubectl (kube.config file).
*NOTE*: Is is important K3S_VERSION on client matches the server otherwise things are likely not to work

# Local Installation

## System requirements

### k3s edge master
* Local linux box, AWS EC2 VM instance or Google Cloud Platform GCE VM instance
* OS: Ubuntu 18.04
* Architecture: amd64
* CPU: at least 1vcpu
* RAM: At least 3.75GB
* Storage: At least 10GB

### dev machine
* User's desktop, capable of ssh'ing to the k3s edge master

## Network topology
* The k3s master and the dev machine both need access to the Internet.
* The dev machine needs to be able to `ssh` and `scp` into the k3s master.
* The k3s master needs to have port 6443 open for k3s.
* The edge node needs to have access to port 6443 in the k3s master.

## Setting k3s master up
Run the following commands on the k3s master machine.
Retrieve the scripts:
```
git clone https://gitlab.com/arm-research/smarter/example.git
```
**NOTE:** The user needs `sudo` privileges and might need to input password.
```
./example/scripts/edge-k3s-setup.sh
```
At the end of the script, you should see the message:
```
kubectl cluster-info
Kubernetes master is running at https://x.x.x.x:6443
```
This indicates that the master was installed successfully.  
The `edge-k3s-setup.sh` script also creates the following files four files (`server_ip`, `token`, `k3s_version` and `edge-kube.config`) that are used to setup other nodes and control the cluster. **COPY THESE FILES TO THE DEV MACHINE**  
At this point, the k3s master is running!

# Joining a non-yocto k3s node
To join an arm64 node which does not use our yocto build for Xavier or Rpi4, you can simply ssh into the node you wish to join and perform the following:

Register node with k3s edge master
```bash
export K3S_VERSION="v1.18.3+k3s1"
export K3S_TOKEN=<your token>
export K3S_SERVER_IP=<your k3s server ip>
```
```bash
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=$K3S_VERSION K3S_URL=https://$K3S_SERVER_IP:6443 K3S_TOKEN=$K3S_TOKEN sh -s - --docker --no-flannel --kubelet-arg cluster-dns=169.254.0.2
sleep 5
sudo systemctl stop k3s-agent
wget https://gitlab.com/arm-research/smarter/k3s/-/jobs/702137534/artifacts/raw/v1.18.3-k3s2-smarter/arm64/k3s-arm64
sudo chmod +x k3s-arm64
sudo mv k3s-arm64 /usr/local/bin/k3s
sudo systemctl start k3s-agent
```

*NOTE*: Is is important K3S_VERSION on client matches the server otherwise things are likely not to work

# Firewall

Make sure you open port 6443 in your firewall so external hosts can contact your new master.
On AWS, you will need to do this by editing the security group policy and adding an inbound rule.
